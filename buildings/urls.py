from django.conf.urls import patterns, include, url
from rest_framework.urlpatterns import format_suffix_patterns
from buildings import views

urlpatterns = patterns('',
    url(r'^e/$', views.UserListView.as_view(), name='e'),
    # url(r'^employee/(?P<pkk>[0-9]+)/$', views.EmployeePut.as_view()),
    # url(r'^game/$', views.GameList.as_view()),
    # url(r'^game/(?P<pkk>[0-9]+)/$', views.GameSel.as_view()),
)

# urlpatterns = format_suffix_patterns(urlpatterns)