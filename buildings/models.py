from django.db import models
from django.core.validators import RegexValidator, MinValueValidator, MaxValueValidator
from django.core.exceptions import ValidationError

class Building_Main(models.Model):
	building_id = models.IntegerField(unique=True)
	building_name = models.CharField(max_length=30)
	building_add = models.CharField(max_length=120)
	building_designer = models.CharField(max_length=20)
	building_img = models.ImageField(upload_to=None,blank=True)

	def __str__(self):
		return self.building_name

	class Meta:
		ordering = ('building_name',)


class Building_Part(models.Model):
	part_id = models.IntegerField(unique=True)
	###### Regular Expression ##########
	cap_alp=RegexValidator(regex=r'^[A-Z]{1}$',message="Please Enter the Building Number (For e.g. A or B)")
	part_name = models.CharField(max_length=1,validators=[cap_alp],unique=True)
	####### End of Use of Regular Expression #########
	part_total_floor = models.IntegerField(default=0)
	part_avil = models.IntegerField(default=0)
	part_sold = models.IntegerField(default=0)
	part_building_main = models.ForeignKey(Building_Main)
	part_img = models.ImageField(upload_to=None,blank=True)

	def __str__(self):
		return self.part_name

	class Meta:
		ordering = ('part_name',)

class Floor_Group(models.Model):
	num=RegexValidator(regex=r'^[A-Z]{1}[0-9]{2}$',message="Please Enter the Buiding Number (i.e. between 0 to 9)")
	floor_id = models.IntegerField(unique=True,validators=[num,MinValueValidator(0),MaxValueValidator(99)])
	floor_group_name = models.CharField(max_length=8,default='0-0')
	floor_avil = models.IntegerField(default=0)
	floor_sold = models.IntegerField(default=0)
	floor_building_part = models.ForeignKey(Building_Part)
	group_img = models.ImageField(upload_to=None,blank=True)

	def __str__(self):
		return self.floor_group_name +" for Building "+str(self.floor_building_part)

	class Meta:
		ordering = ('floor_id',)

class Floor_Each(models.Model):
	# val=RegexValidator(regex=r'^[low-high]',message="Are u Mad")
	efloor_number = models.IntegerField(default=0)
	a='1BHK'
	b='2BHK'
	c='3BHK'
	d='4BHK'
	e='5BHK'
	CHOICES = (
		(a, '1BHK'),
		(b, '2BHK'),
		(c, '3BHK'),
		(d, '4BHK'),
		(e, '5BHK'),
	)
	efloor_type = models.CharField(max_length=4,
									choices=CHOICES,
									default=c)
	floor_img = models.ImageField(upload_to=None,blank=True)
	efloor_avil = models.IntegerField(default=0)
	efloor_sold = models.IntegerField(default=0)
	floor_group = models.ForeignKey(Floor_Group)#,validators=[val])
	
	def __str__(self):
		return self.efloor_type

	def save(self, *args, **kwargs):
		inp = self.efloor_number
		strng = str(self.floor_group)
		num = strng.split("-")
		low=int(num[0])
		num=num[1].split(" for")
		print "val   awedfdf    ===== ",num[0]
		high=int(num[0])
		print 'i value',low,"    ",high
		if low<=inp :
			if high>=inp:
				super(Floor_Each, self).save(*args, **kwargs) # Call the "real" save() method.
			else:
				raise ValidationError(('Invalid Selection for floor Group'), code='invalid')	
		else:
			raise ValidationError(('Invalid Selection for floor Group'), code='invalid')	

class Flat_Distribution(models.Model):
	flat_number = models.IntegerField(default=0, unique=True)
	a='AVAILABLE'
	b='UNAVAILABLE'
	c='ON HOLD'
	CHOICES = (
		(a, 'AVAILABLE'),
		(b, 'UNAVAILABLE'),
		(c, 'ON HOLD'),
	)
	flat_status = models.CharField(max_length=11,choices=CHOICES)
	flat_area = models.CharField(max_length=20)
	flat_price = models.IntegerField(default=0)
	flat_coordinates = models.CharField(max_length=50,unique=True,default='')
	floor_each = models.ForeignKey(Floor_Each)
	flat_img = models.ImageField(upload_to=None,blank=True)

	def __str__(self):
		return str(self.flat_number)


class Flat_Interior(models.Model):
	flat_booking_price = models.IntegerField(default=0)
	a='Refundable'
	b='Not Refundable'
	c='Partial Refundable'
	CHOICES = (
		(a, 'Refundable'),
		(b, 'Not Refundable'),
		(c, 'Partial Refundable'),
	)
	flat_status = models.CharField(max_length=11,choices=CHOICES,default=a)
	flat_distribution = models.ForeignKey(Flat_Distribution)
	interior_img = models.ImageField(upload_to=None,blank=True)

	def __str__(self):
		return str(self.flat_booking_price)
