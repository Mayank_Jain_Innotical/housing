from django.contrib import admin
from .models import *


class Test(admin.ModelAdmin):
	def has_add_permission(self, request):
		return False

admin.site.register(Building_Main)
admin.site.register(Building_Part)
admin.site.register(Floor_Group,Test)
admin.site.register(Floor_Each)
admin.site.register(Flat_Distribution)
admin.site.register(Flat_Interior)

