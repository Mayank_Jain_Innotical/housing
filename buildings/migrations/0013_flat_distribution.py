# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('buildings', '0012_auto_20150724_0732'),
    ]

    operations = [
        migrations.CreateModel(
            name='Flat_Distribution',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('flat_number', models.IntegerField(default=0, unique=True)),
                ('flat_status', models.CharField(max_length=11, choices=[(b'AVAILABLE', b'AVAILABLE'), (b'UNAVAILABLE', b'UNAVAILABLE'), (b'ON HOLD', b'ON HOLD')])),
                ('flat_area', models.CharField(max_length=20)),
                ('flat_price', models.IntegerField(default=0)),
                ('floor_each', models.ForeignKey(to='buildings.Floor_Each')),
            ],
        ),
    ]
