# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('buildings', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='floor_group',
            name='floor_group_name',
            field=models.CharField(default=b'0-0', max_length=8),
        ),
    ]
