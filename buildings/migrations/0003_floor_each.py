# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('buildings', '0002_floor_group_floor_group_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='Floor_Each',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('efloor_number', models.IntegerField(default=0)),
                ('efloor_type', models.CharField(max_length=4, validators=[django.core.validators.RegexValidator(regex=b'^[1-5]{1}$', message=b'Please Enter the Buiding Number (i.e. between 0 to 9)')])),
                ('efloor_avil', models.IntegerField(default=0)),
                ('efloor_sold', models.IntegerField(default=0)),
                ('floor_group', models.ForeignKey(to='buildings.Floor_Group')),
            ],
        ),
    ]
