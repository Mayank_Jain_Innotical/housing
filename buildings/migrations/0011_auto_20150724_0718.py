# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('buildings', '0010_auto_20150724_0713'),
    ]

    operations = [
        migrations.AlterField(
            model_name='floor_each',
            name='efloor_type',
            field=models.CharField(default=1, max_length=4, choices=[(1, b'1BHK'), (2, b'2BHK'), (3, b'3BHK'), (4, b'4BHK'), (5, b'5BHK')]),
        ),
    ]
