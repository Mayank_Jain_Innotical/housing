# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('buildings', '0013_flat_distribution'),
    ]

    operations = [
        migrations.CreateModel(
            name='Flat_Interior',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('flat_booking_price', models.IntegerField(default=0)),
                ('flat_status', models.CharField(default=b'Refundable', max_length=11, choices=[(b'Refundable', b'Refundable'), (b'Not Refundable', b'Not Refundable'), (b'Partial Refundable', b'Partial Refundable')])),
            ],
        ),
        migrations.AddField(
            model_name='flat_distribution',
            name='flat_coordinates',
            field=models.CharField(default=b'', unique=True, max_length=50),
        ),
        migrations.AlterField(
            model_name='building_main',
            name='building_img',
            field=models.ImageField(upload_to=None, blank=True),
        ),
        migrations.AddField(
            model_name='flat_interior',
            name='flat_distribution',
            field=models.ForeignKey(to='buildings.Flat_Distribution'),
        ),
    ]
