# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('buildings', '0009_auto_20150724_0701'),
    ]

    operations = [
        migrations.AlterField(
            model_name='floor_each',
            name='efloor_type',
            field=models.CharField(default=1, max_length=4, choices=[(1, b'Freshman'), (2, b'Sophomore'), (3, b'Junior'), (4, b'Senior'), (5, b'Senior')]),
        ),
    ]
