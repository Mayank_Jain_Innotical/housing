# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('buildings', '0007_auto_20150724_0614'),
    ]

    operations = [
        migrations.AlterField(
            model_name='floor_group',
            name='floor_id',
            field=models.IntegerField(validators=[django.core.validators.RegexValidator(regex=b'^[A-Z]{1}[0-9]{2}$', message=b'Please Enter the Buiding Number (i.e. between 0 to 9)'), django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(99)]),
        ),
    ]
