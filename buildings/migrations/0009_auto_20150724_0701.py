# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('buildings', '0008_auto_20150724_0619'),
    ]

    operations = [
        migrations.AlterField(
            model_name='building_part',
            name='part_name',
            field=models.CharField(unique=True, max_length=1, validators=[django.core.validators.RegexValidator(regex=b'^[A-Z]{1}$', message=b'Please Enter the Building Number (For e.g. A or B)')]),
        ),
        migrations.AlterField(
            model_name='floor_group',
            name='floor_id',
            field=models.IntegerField(unique=True, validators=[django.core.validators.RegexValidator(regex=b'^[A-Z]{1}[0-9]{2}$', message=b'Please Enter the Buiding Number (i.e. between 0 to 9)'), django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(99)]),
        ),
    ]
