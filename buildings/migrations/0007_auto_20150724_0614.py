# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('buildings', '0006_auto_20150724_0538'),
    ]

    operations = [
        migrations.AlterField(
            model_name='floor_each',
            name='efloor_type',
            field=models.CharField(max_length=4, validators=[django.core.validators.RegexValidator(regex=b'^[1-5]', message=b'Please Enter the Buiding Number (i.e. between 0 to 9)')]),
        ),
    ]
