# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('buildings', '0014_auto_20150724_1007'),
    ]

    operations = [
        migrations.AddField(
            model_name='building_part',
            name='part_img',
            field=models.ImageField(upload_to=None, blank=True),
        ),
        migrations.AddField(
            model_name='flat_distribution',
            name='flat_img',
            field=models.ImageField(upload_to=None, blank=True),
        ),
        migrations.AddField(
            model_name='flat_interior',
            name='interior_img',
            field=models.ImageField(upload_to=None, blank=True),
        ),
        migrations.AddField(
            model_name='floor_each',
            name='floor_img',
            field=models.ImageField(upload_to=None, blank=True),
        ),
        migrations.AddField(
            model_name='floor_group',
            name='group_img',
            field=models.ImageField(upload_to=None, blank=True),
        ),
    ]
