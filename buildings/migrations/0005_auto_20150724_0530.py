# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('buildings', '0004_auto_20150723_0935'),
    ]

    operations = [
        migrations.RenameField(
            model_name='floor_each',
            old_name='floor_group',
            new_name='efloor_group',
        ),
    ]
