# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Building_Main',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('building_id', models.IntegerField(unique=True)),
                ('building_name', models.CharField(max_length=30)),
                ('building_add', models.CharField(max_length=120)),
                ('building_designer', models.CharField(max_length=20)),
                ('building_img', models.ImageField(upload_to=None)),
            ],
            options={
                'ordering': ('building_name',),
            },
        ),
        migrations.CreateModel(
            name='Building_Part',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('part_id', models.IntegerField(unique=True)),
                ('part_name', models.CharField(unique=True, max_length=1, validators=[django.core.validators.RegexValidator(regex=b'^[A-Z]{1}$', message=b'Please Enter the Buiding Number (For e.g. A or B)')])),
                ('part_total_floor', models.IntegerField(default=0)),
                ('part_avil', models.IntegerField(default=0)),
                ('part_sold', models.IntegerField(default=0)),
                ('part_building_main', models.ForeignKey(to='buildings.Building_Main')),
            ],
            options={
                'ordering': ('part_name',),
            },
        ),
        migrations.CreateModel(
            name='Floor_Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('floor_id', models.IntegerField(unique=True, validators=[django.core.validators.RegexValidator(regex=b'^[0-9]{2}$', message=b'Please Enter the Buiding Number (i.e. between 0 to 9)'), django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(99)])),
                ('floor_avil', models.IntegerField(default=0)),
                ('floor_sold', models.IntegerField(default=0)),
                ('floor_building_part', models.ForeignKey(to='buildings.Building_Part')),
            ],
            options={
                'ordering': ('floor_id',),
            },
        ),
    ]
