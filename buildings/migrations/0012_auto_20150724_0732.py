# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('buildings', '0011_auto_20150724_0718'),
    ]

    operations = [
        migrations.AlterField(
            model_name='floor_each',
            name='efloor_type',
            field=models.CharField(default=b'3BHK', max_length=4, choices=[(b'1BHK', b'1BHK'), (b'2BHK', b'2BHK'), (b'3BHK', b'3BHK'), (b'4BHK', b'4BHK'), (b'5BHK', b'5BHK')]),
        ),
    ]
