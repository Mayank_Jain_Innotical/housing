# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('buildings', '0005_auto_20150724_0530'),
    ]

    operations = [
        migrations.RenameField(
            model_name='floor_each',
            old_name='efloor_group',
            new_name='floor_group',
        ),
    ]
